#! /usr/bin/env python
from setuptools import find_packages, setup

try:  # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError:  # for pip <= 9.0.3
    from pip.req import parse_requirements

requires = [str(ir.req) for ir in parse_requirements('requirements.txt',
                                                     session='No Session')]

setup(
    name="chordpro-converters",
    use_scm_version=True,
    description="Libraries and CLI tools to convert other chord formats to chordpro",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
    ],
    author="Nick Shobe",
    author_email="nickshobe@gmail.com",
    license="MIT License",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    setup_requires=["setuptools-scm"],
    # extras_require={"test": tests_require},
    # tests_require=tests_require,
    entry_points={"console_scripts": ["txt-to-pro=converters.txt_to_pro:main",
                                      "warden-scraper=converters.warden_scraper:main"]},
)
