#!/usr/bin/env python
import re
import sys

from argparse import ArgumentParser
from os import path


def parse_args():

    parser = ArgumentParser('txt-to-pro')

    parser.add_argument('input',
                        nargs='?',
                        default='src.cho',
                        help='Input txt file foratted with'
                             'chord above charecter it will impact')

    parser.add_argument('--output',
                        help='outout chordpro formatted file')

    parser.add_argument('--chord-defs',
                        default='./chords.txt',
                        help='File with chord defs comman seps')

    parser.add_argument('--ug',
                        action='store_true',
                        help='Ultimate-guitar.com formatted song.'
                             'UG chord songs often fallow a simalar pattern.')

    return parser.parse_args()


def expand_path(file_path):
    return path.expandvars(path.expanduser(path.abspath(file_path)))


class TextToPro(object):

    def __init__(self,
                 chord_defs: list,
                 format: str = 'two_line_positional'):
        self.chord_defs = chord_defs

        self.format = format

    def is_chord_line(self, line, chorddefs):
        """Best effort guess that this is a line of chords"""
        clean_line = line.rstrip()
        if not len(clean_line):
            return False

        chords = 0
        spaces = 0
        unmatched = 0
        for item in clean_line.split(' '):
            if not len(item):
                spaces += 1
            elif item in chorddefs:
                chords += 1
            else:
                unmatched += 1

        # one chord line
        if chords == 1 and unmatched == 0:
            return True
        elif chords >= unmatched:
            return True
        else:
            return False

    def find_chords(self, line):
        chords = [ch.rstrip() for ch in line.split(' ')
                  if len(ch) and ch != '\n']
        return chords, self.chord_indexes(chords, line)

    def chord_indexes(self, chords, line):
        indx = 0
        indexes = []
        for chord in chords:
            indx = line.find(chord, indx)
            indexes.append(indx)
            indx = indx + len(chord)

        return indexes

    def parse(self, lines: list, chord_defs: list = None):
        if not chord_defs:
            chord_defs = self.chord_defs

        if self.format == 'ultimate_guitar':
            ug_section = re.compile(r'^\s*\[(.+)\][^a-zA-Z]')

        outlines = []

        prev_line = ''
        for line in lines:
            if self.is_chord_line(prev_line, chord_defs):

                if len(prev_line) > len(line):
                    # extend line to fit chords by position
                    new_line = (line.rstrip()
                                + ' ' * (len(prev_line) - len(line))
                                + '\n')
                else:
                    new_line = line

                # find all chords and their start indexes
                chords, chord_indexes = self.find_chords(prev_line)

                # reverse the chords
                chords_rev = list(reversed(chords))

                # Because the string grows in length due to chords being inserted
                # into the string and indexes are calculated from 0/start,
                # the replacement must be done from the end first.
                # Otherwise chord indexes would need to be recalculated each iteration.
                for ch, chord_indx in enumerate(reversed(chord_indexes)):
                    front = new_line[:chord_indx]
                    back = new_line[chord_indx:]
                    new_line = f'{front}[{chords_rev[ch]}]{back}'

                outlines.append(new_line)
            else:
                if not self.is_chord_line(line, chord_defs):
                    if self.format == 'ultimate_guitar':
                        groups = ug_section.findall(line)
                        if len(groups):
                            line = line.replace('[{}]'.format(groups[0]),
                                                '{{c:{}}}'.format(groups[0]))
                    outlines.append(line)

            prev_line = line

        return outlines

    @staticmethod
    def load_chord_defs(path: str) -> list:
        with open(expand_path(path), 'r') as chord_file:
            return [l.rstrip() for l in chord_file.readlines()]


def main():
    args = parse_args()

    format = 'ultimate_guitar' if args.ug else 'two_line_positional'

    tp = TextToPro(TextToPro.load_chord_defs(args.chord_defs),
                   format=format)

    with open(expand_path(args.input), 'r') as infile:
        lines = infile.readlines()

    sys.stdout.writelines(tp.parse(lines))
