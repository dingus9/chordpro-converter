#!/usr/bin/env python
import os
import requests

from bs4 import BeautifulSoup
from .txt_to_pro import TextToPro

"""
    Scrapes the site http://www.bradwarden.com/music/hymnchords/
    and converts two line formatted chords to chordpro
"""


class DOM(object):

    def __init__(self, html: str) -> object:
        self.html = html
        self.document = BeautifulSoup(html, "lxml")

    def S(self, selector: str):
        return self.document.select(selector)


def song_list(document: DOM) -> list:
    """
    Scrapes the site main page for the list of songs
    :param uri str:
    :return: A list of existing songs
    """
    return document.S('a[href*="./?num="]')


def song_data(document: DOM) -> str:
    """
    Scrapes each sub page dom for the chord content and returns it as a string
    """
    return document.S('pre')[0].text


def main():
    output_dir = './tmp'
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    url = 'http://www.bradwarden.com/music/hymnchords/'
    resp = requests.get(url,
                        headers={'User-Agent': 'curl/7.54.0'})
    resp.raise_for_status()
    dom = DOM(resp.text)

    song_tags = song_list(dom)
    songs = {x.attrs['href'].split('=')[1]:
             {'title': x.text,
              'href': x.attrs['href']}
             for x in song_tags}

    tp = tp = TextToPro(TextToPro.load_chord_defs('chords.txt'),
                        format='two_line_positional')

    for idx, song in songs.items():
        resp = requests.get(f'{url}{song["href"]}',
                            headers={'User-Agent': 'curl/7.54.0'})
        resp.raise_for_status()
        header = '{title:' + song['title'] + '}\n\n'
        lines = tp.parse([x + '\n'
                          for x in song_data(DOM(resp.text)).splitlines()
                          if len(x)])
        filename = song["title"].replace(' ', '_').replace('.', '') + '.cho'
        with open(os.path.join(output_dir, filename), 'w') as outfile:
            outfile.write(header)
            outfile.writelines(lines)
